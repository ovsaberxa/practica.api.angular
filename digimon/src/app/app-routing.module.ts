import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'digimones', loadChildren:()  => import('./app.module').then(m => m.AppModule)},
  { path: 'digimon', loadChildren: () => import('./modules/digimon/digimon.module').then(m => m.DigimonModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
