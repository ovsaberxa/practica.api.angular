import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DigimonesComponent } from './pages/digimones/digimones.component';

const routes: Routes = [
  {path: 'lista',component:DigimonesComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DigimonRoutingModule { }
