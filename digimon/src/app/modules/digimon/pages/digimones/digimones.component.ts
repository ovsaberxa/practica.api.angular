import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs';
import { DigimonesServices } from 'src/app/core/services/digimones.service';
@Component({
  selector: 'app-digimones',
  templateUrl: './digimones.component.html',
  styleUrls: ['./digimones.component.css']
})
export class DigimonesComponent implements OnInit {

  //Declar variable digimones
  digimones: any;
  filter = '';
  page:number = 0;
  //constructor llamando al servicio
  constructor(private digimon: DigimonesServices) { }

  //Llamar al servicio asignandole el valor
  ngOnInit() {
    this.digimon.getDigimones().subscribe(
      (response) => { this.digimones = response; console.log(response) },
      (error) => { console.error(error) }
    )
  }


}
