import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'digimonFilter'
})

export class DigimonPipe implements PipeTransform {
  transform(value: any, arg: any): any {
    const resultDigimon = [];
    for (const digimon of value){
      if (digimon.name.toLowerCase().indexOf(arg.toLowerCase()) > -1){
        resultDigimon.push(digimon);
      } 
    }
    return resultDigimon;
  }
}
