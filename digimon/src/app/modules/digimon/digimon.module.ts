import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DigimonRoutingModule } from './digimon-routing.module';
import { DigimonesComponent } from './pages/digimones/digimones.component';
import { DigimonPipe } from './pages/digimones/digimon.pipe';
import { FormsModule } from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';


@NgModule({
  declarations: [
    DigimonesComponent,
    DigimonPipe
  ],
  imports: [
    CommonModule,
    DigimonRoutingModule,
    FormsModule,
    NgxPaginationModule
  ]
})
export class DigimonModule { }
