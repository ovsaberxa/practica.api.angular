import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http'

@Injectable()

export class DigimonesServices {
    //URL API
    private API_SERVER = "https://digimon-api.vercel.app/api/digimon";

    //Constructor
    constructor(public http: HttpClient) { }
    
    //metodo para que enliste
    public getDigimones(): Observable<any> {
        return this.http.get(this.API_SERVER);
    }
}
